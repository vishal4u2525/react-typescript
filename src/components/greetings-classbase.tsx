import React from "react";

type GreetingsClassBaseProps = {
  value?: number;
  name: string;
}

type GreetingsClassBaseState = {
  value: number;
}

class GreetingsClassBase extends React.Component<GreetingsClassBaseProps, GreetingsClassBaseState> {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    };
  }

  increaseValue = (e: React.MouseEvent<HTMLButtonElement>) => {
    console.log(e.clientX);
    // This is not the correct way to increment the state. This is just for the sake of this example.
    // const value = this.state.value + 1;
    // this.setState({ value });
  }

  render() {
    const { value } = this.state;

    return (
      <React.Fragment>
        <div>value: {value}</div>
        <button onClick={(e) => this.increaseValue}>increase</button>
      </React.Fragment>
    );
  }
}

export default GreetingsClassBase;
