import React from 'react';

type GreetingsProps = {
  a: number;
  b: number;
}

const Greetings: React.FC<GreetingsProps> = (props) => {
  console.log(props);
  return (
    <div>
      <div>Greetings functional component</div>
    </div>
  );
}

export default Greetings;
