import React from 'react';
import GreetingsClassBase from "./components/greetings-classbase";
import Greetings from "./components/greetings";

const hello = 'hello';

const App = () => {
  return (
    <React.Fragment>
      <div>App</div>
      <div>
        <GreetingsClassBase name={hello}>
          <div>hello</div>
        </GreetingsClassBase>
      </div>
      <div>
        <Greetings a={1} b={2}>hola</Greetings>
      </div>
    </React.Fragment>
  );
}

export default App;
